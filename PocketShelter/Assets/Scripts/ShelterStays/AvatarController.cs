using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class AvatarController : MonoBehaviour {
        [SerializeField]
        private Animator avatarAnimator;

        [Tooltip("Velocidad de movimiento")]
        public float speed;

        [Tooltip("Posición mínima en el eje Y que el avatar puede lograr")]
        public float minY;
        [Tooltip("Posición máxima en el eje Y que el avatar puede lograr")]
        public float maxY;

        [Tooltip("Tamaño mínimo del avatar, justo cuando se encuentre en la mínima posición del eje Y")]
        public float minScale;
        [Tooltip("Tamaño máximo del avatar, justo cuando se encuentre en la máxima posición del eje Y")]
        public float maxScale;

        // Constante de modificación entre el tamaño y la posición del avatar
        private float scaleModifier;

        // Variables auxiliares para el movimiento del avatar
        private Vector3 targetPosition;
        private Vector3 targetDirection;

        private void Start() {
            // Inicialización de variables
            scaleModifier = 1 / (Mathf.Abs(minY) + Mathf.Abs(maxY));
            targetPosition = Vector3.zero;
            targetDirection = Vector3.zero;

            // TODO Colocación del avatar en la escena

            // Ajuste del tamaño
            transform.localScale = Mathf.Lerp(maxScale, minScale, (transform.position.y + Mathf.Abs(minY)) * scaleModifier) * Vector3.one;
        }

        private void OnEnable() {
            // Activa suscripción a eventos
            InputManager.ScreenClick += MoveToScreenPosition;
        }

        private void OnDisable() {
            // Desactiva suscripción a eventos
            InputManager.ScreenClick -= MoveToScreenPosition;
        }

        private void Update() {
            // Comprueba si el avatar debe moverse
            if (!targetDirection.Equals(Vector3.zero)) {
                // Lleva a cabo el movimiento
                transform.Translate(targetDirection * speed * Time.deltaTime);

                // Modificación del tamaño, solo si se mueve verticalmente
                if (targetDirection.y != 0) {
                    transform.localScale = Mathf.Lerp(maxScale, minScale, (transform.position.y + Mathf.Abs(minY)) * scaleModifier) * Vector3.one;
                }

                // Sentido en que debe mirar el avatar, según si el destino se encuentra a su derecha o a su izquierda
                Vector3 scale = transform.localScale;
                if (targetDirection.x >= 0) {
                    scale.x = Mathf.Abs(scale.x);
                } else {
                    scale.x = -Mathf.Abs(scale.x);
                }
                transform.localScale = scale;

                // Comprueba si el avatar ha llegado ya a su destino
                if (Vector3.Distance((Vector3)targetPosition, transform.position) < 0.1f) {
                    targetPosition = Vector3.zero;
                    targetDirection = Vector3.zero;
                    avatarAnimator.SetBool("movement", false);
                }
            }
        }

        private void MoveToScreenPosition(Vector2 screenPosition) {
            // Comprueba si la posición marcada en pantalla es válida para el desplazamiento del avatar
            Collider2D overlapCollider = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(screenPosition), LayerMask.GetMask("WalkableFloor", "UI", "Animal"));
            if ((overlapCollider != null) && (overlapCollider.CompareTag("WalkableFloor"))) {
                // Cálculo de la posición de destino, así como de la dirección que debe tomar
                targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                targetPosition.z = 0;
                targetDirection = (targetPosition - transform.position).normalized;

                // TODO Velocidades de movimiento distintas para ejes X e Y

                // Ejecuta la animación de movimiento
                avatarAnimator.SetBool("movement", true);
            }
        }
    }

}