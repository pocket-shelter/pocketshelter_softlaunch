using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class AnimalController : MonoBehaviour {

        private ShelterStayUIController shelterUI;

        #region Unity Messages

        private void Start() {
            shelterUI = GameObject.FindObjectOfType<ShelterStayUIController>();
        }

        private void OnEnable() {
            // Activa suscripcion a eventos
            InputManager.ScreenClick += SelectAnimal;
        }

        private void OnDisable() {
            // Desactiva suscripcion a eventos
            InputManager.ScreenClick -= SelectAnimal;
        }

        #endregion

        private void SelectAnimal(Vector2 screenPosition) {
            // Comprueba si la posicion marcada en pantalla se corresponde con este animal en concreto
            Collider2D overlapCollider = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(screenPosition), LayerMask.GetMask("Animal"));
            if ((overlapCollider != null) && (overlapCollider.gameObject.Equals(gameObject))) {
                Debug.Log("Soy " + name);
                shelterUI.OpenAnimalSelectionPanel(name);
            }
        }
    }

}
