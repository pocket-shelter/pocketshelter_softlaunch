using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter
{

    public class ShelterStayController : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private GameObject catPrefab;
        [SerializeField]
        private GameObject dogPrefab;

        [Header("Shelter Stay Info")]
        [SerializeField]
        private List<Transform> spawningPositions;

        private void Start()
        {
            // Distribucion de los animales por la estancia
            GameObject animalObject;
            Animal animalInfo;
            int pos;
            foreach (string animalID in GameManager.SessionController.GetAnimalsForCurrentBuilding())
            {
                // Creacion del animal
                animalInfo = GameManager.GameData.GetAnimal(animalID);
                switch (animalInfo.animal)
                {
                    case Animal.Type.Cat:
                        animalObject = catPrefab;
                        break;
                    case Animal.Type.Dog:
                        animalObject = dogPrefab;
                        break;
                    default:
                        // Por defecto es perro, aunque no deberia darse este caso
                        animalObject = dogPrefab;
                        break;
                }
                // Se crea el animal en una posicion valida aleatoria y esta se elimina para no ser repetida
                pos = Random.Range(0, spawningPositions.Count);
                animalObject = Instantiate(animalObject, spawningPositions[pos].position, Quaternion.identity, transform);
                spawningPositions.RemoveAt(pos);

                // Se ha creado como hijo del controlador para que aparezca en la escena adecuada
                animalObject.transform.SetParent(null);

                // Configuracion del animal
                animalObject.name = animalID;
            }
        }
    }

}
