using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class CameraController : MonoBehaviour {
        [Tooltip("Posición mínima válida en el eje X para la cámara")]
        public float minX;
        [Tooltip("Posición máxima válida en el eje X para la cámara")]
        public float maxX;

        [Tooltip("Velocidad de movimiento de la cámara")]
        public float speed;

        private void OnEnable() {
            InputManager.CameraMovement += MoveCamera;
        }

        private void OnDisable() {
            InputManager.CameraMovement -= MoveCamera;
        }

        private void Update() {
            // TODO Notificar del movimiento de la cámara
            //if ((transform.position.x > minX) && (Input.mousePosition.x < 100)) {
            //    transform.position += speed * Time.deltaTime * Vector3.left;
            //} else if ((transform.position.x < maxX) && (Input.mousePosition.x > 1820) && (Input.mousePosition.x < 1920)) {
            //    transform.position += speed * Time.deltaTime * Vector3.right;
            //}
        }

        private void MoveCamera(Vector2 movement) {
            // Elimina el movimiento vertical
            movement.y = 0;

            // Lleva a cabo el movimiento de la cámara, asegurándose que no supera los límites
            transform.Translate(movement * speed * Time.deltaTime);
            Vector3 pos = transform.position;
            pos.x = Mathf.Clamp(pos.x, minX, maxX);
            transform.position = pos;
        }

    }

}