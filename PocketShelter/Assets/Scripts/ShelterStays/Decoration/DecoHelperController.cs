using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class DecoHelperController : MonoBehaviour {
        public SpriteRenderer helperSpriteRenderer;
        public SpriteRenderer objectSpriteRenderer;

        public float velocidad;

        public float minX;
        public float maxX;

        public float minY;
        public float maxY;

        public float minScale;
        public float maxScale;

        private Vector2 lastMousePosition;

        private float scaleModifier;

        private Vector3 initialPosition;

        private float onDrag;
        private bool correctPosition;

        private void Start() {
            helperSpriteRenderer.enabled = false;
            scaleModifier = 1/ (Mathf.Abs(minY) + Mathf.Abs(maxY));
            onDrag = 0;
            correctPosition = true;
        }

        private void OnEnable() {
            helperSpriteRenderer.enabled = true;
        }

        private void OnCollisionEnter2D(Collision2D collision) {
            correctPosition = false;
            objectSpriteRenderer.color = Color.red;
        }

        private void OnCollisionExit2D(Collision2D collision) {
            correctPosition = true;
            objectSpriteRenderer.color = Color.white;
        }

        private void OnMouseDown() {
            if (StayDecoController.IsDeleting()) {
                Destroy(transform.parent.gameObject);
            } else {
                initialPosition = transform.parent.position;
                lastMousePosition = Input.mousePosition;
                helperSpriteRenderer.enabled = false;
            }
        }

        private void OnMouseDrag() {
            if (!StayDecoController.IsDeleting()) {
                if (onDrag > 0.5f) {
                    Vector3 pos = Vector3.zero;
                    // Posición X
                    pos.x += (Input.mousePosition.x - lastMousePosition.x) / Screen.width;
                    if ((transform.parent.position.x > minX) && (pos.x < 0)) {

                    } else if ((transform.parent.position.x < maxX) && (pos.x > 0)) {

                    } else {
                        pos.x = 0;
                    }
                    // Posición Y
                    pos.y += (Input.mousePosition.y - lastMousePosition.y) / Screen.height;
                    if ((transform.parent.position.y > minY) && (pos.y < 0)) {

                    } else if ((transform.parent.position.y < maxY) && (pos.y > 0)) {

                    } else {
                        pos.y = 0;
                    }
                    transform.parent.position += velocidad * pos;
                    if (pos.y != 0) {
                        transform.parent.localScale = Mathf.Lerp(maxScale, minScale, (transform.parent.position.y + Mathf.Abs(minY)) * scaleModifier) * Vector3.one;
                    }
                } else {
                    onDrag += Time.deltaTime;
                }
                lastMousePosition = Input.mousePosition;
            }
        }

        private void OnMouseUp() {
            if (!StayDecoController.IsDeleting()) {
                if (!correctPosition) {
                    objectSpriteRenderer.color = Color.white;
                    transform.parent.position = initialPosition;
                }
                helperSpriteRenderer.enabled = true;
                onDrag = 0;
            }
        }
    }

}
