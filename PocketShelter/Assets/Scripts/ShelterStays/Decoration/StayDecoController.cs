using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class StayDecoController : MonoBehaviour {
        private static StayDecoController instance;

        public GameObject startDecoButton;
        public GameObject endDecoButton;

        public GameObject startDeletionButton;
        public GameObject endDeletionButton;

        private List<GameObject> decoHelpers;

        private bool deleting;

        private void Awake() {
            instance = this;

            decoHelpers = new List<GameObject>();
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("DecoHelper")) {
                decoHelpers.Add(obj);
                obj.SetActive(false);
            }
        }

        private void Start() {
            deleting = false;
        }

        public static bool IsDeleting() {
            return instance.deleting;
        }

        public void StartDeleting() {
            deleting = true;
            startDeletionButton.SetActive(false);
            endDeletionButton.SetActive(true);
        }

        public void EndDeleting() {
            deleting = false;
            startDeletionButton.SetActive(true);
            endDeletionButton.SetActive(false);
        }

        public void StartDeco() {
            // Se cambia el esquema de control
            InputManager.Scheme = InputManager.InputScheme.ShelterStay_Deco;

            // Se activa cada uno de los asistentes de los objetos de decoración
            foreach (GameObject dh in decoHelpers) {
                dh.SetActive(true);
            }

            // Actualiza la interfaz del modo decoración
            startDecoButton.SetActive(false);
            startDeletionButton.SetActive(true);
            endDecoButton.SetActive(true);
        }

        public void EndDeco() {
            foreach (GameObject dh in decoHelpers) {
                if (dh != null) {
                    dh.SetActive(false);
                }
            }
            EndDeleting();
            startDeletionButton.SetActive(false);
            startDecoButton.SetActive(true);
            endDecoButton.SetActive(false);
        }
    }

}