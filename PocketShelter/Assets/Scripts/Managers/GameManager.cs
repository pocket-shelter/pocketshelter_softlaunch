using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class GameManager : MonoBehaviour {
        private static GameManager instance;

        [SerializeField]
        private PocketShelterData gameData;

        [SerializeField]
        private ResourcesManager resourcesManager;

        [SerializeField]
        private SceneController sceneController;

        [SerializeField]
        private SessionController sessionController;

        [SerializeField]
        private PlayerData playerData;

        public static PocketShelterData GameData
        {
            get
            {
                return instance.gameData;
            }
        }

        public static ResourcesManager ResourcesManager {
            get {
                return instance.resourcesManager;
            }
        }

        public static SceneController SceneController {
            get {
                return instance.sceneController;
            }
        }

        public static SessionController SessionController
        {
            get
            {
                return instance.sessionController;
            }
        }

        public static PlayerData PlayerData {
            get {
                return instance.playerData;
            }
        }

        private void Awake() {
            instance = this;
        }

        private void Start() {
            resourcesManager.SetInitialCount(playerData.coins, playerData.solidaryCoins);
        }

        public static void LoadBuilding(string ID) {
            instance.StartCoroutine(instance.sceneController.LoadBuilding(ID));
        }

        public static void LoadInterface()
        {
            instance.StartCoroutine(instance.LoadInterface(true));
        }

        public static void LoadMap() {
            instance.StartCoroutine(instance.LoadMapScenes());
        }

        public static void LoadMinigames()
        {
            instance.StartCoroutine(instance.LoadMinigamesScene());
        }

        public static void LoadShelterStay(string ID) {
            instance.sessionController.currentBuilding = ID;
            instance.StartCoroutine(instance.sceneController.LoadShelterStay(ID));
        }

        public static void LoadResourcesShop() {
            instance.StartCoroutine(instance.LoadResourcesShopScene());
        }

        public static void UnloadResourcesShop() {
            instance.StartCoroutine(instance.sceneController.UnloadResourceShop());
        }

        private IEnumerator LoadInterface(bool loadMap)
        {
            yield return sceneController.LoadMainInterface(loadMap);
        }

        private IEnumerator LoadMapScenes() {
            yield return sceneController.LoadMap();
        }

        private IEnumerator LoadMinigamesScene()
        {
            yield return sceneController.LoadMinigames(); ;
        }

        private IEnumerator LoadResourcesShopScene() {
            yield return sceneController.LoadResourceShop();
        }

    }

}