using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class ResourceShopManager : MonoBehaviour {

        [SerializeField]
        private GameObject coinsPanel;

        [SerializeField]
        private GameObject solidaryCoinsPanel;

        public void BuyCoins(int count) {
            GameManager.ResourcesManager.AddCoins(count);
        }

        public void BuySolidaryCoins(int count) {
            GameManager.ResourcesManager.AddSolidaryCoins(count);
        }

        public void CloseShop() {
            GameManager.UnloadResourcesShop();
        }

        public void ShowCoinsPanel() {
            coinsPanel.SetActive(true);
            solidaryCoinsPanel.SetActive(false);
        }

        public void ShowSolidaryCoinsPanel() {
            coinsPanel.SetActive(false);
            solidaryCoinsPanel.SetActive(true);
        }

    }

}