using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

namespace PerlaOculta.PocketShelter {

    public class SceneController : MonoBehaviour {
        /* Eventos de notificación */
        public delegate void SceneNotification(string scene);
        /// <summary>
        /// Notificación cuando se carga una escena
        /// </summary>
        public SceneNotification SceneLoaded;
        /// <summary>
        /// Notificación cuando se descarga una escena
        /// </summary>
        public SceneNotification SceneUnloaded;

        [SerializeField]
        private Image blackFadeImage;

        [SerializeField]
        private float fadeDuration;

        /// <summary>
        /// Carga la escena correspondiente al edificio a visitar (async)
        /// </summary>
        /// <param name="ID">Identificador del edificio</param>
        /// <returns>Devuelve cuando se termina de cargar la escena</returns>
        public IEnumerator LoadBuilding(string ID) {
            // Inicio de fundido a negro para suavizar la carga
            yield return StartFade();

            // Descarga todas las escenas, salvo la de gestion (Main) y la de interfaz (Interface)
            yield return WaitForUnloadScene("ResourcesShop");
            yield return WaitForUnloadScene("Map");
            // Carga la escena del edificio
            yield return WaitForLoadScene(ID);

            // Fin de fundido a negro para suavizar la carga
            yield return EndFade();
        }

        /// <summary>
        /// Carga las escenas correspondientes al juego principal (async)
        /// </summary>
        /// <returns>Devuelve cuando se terminan de cargar todas las escenas relacionadas</returns>
        public IEnumerator LoadGame() {
            // Carga/Descarga de las escenas
            yield return WaitForLoadScene("Main");
            yield return WaitForLoadScene("StartMenu");
            yield return WaitForUnloadScene("Launcher");
        }

        /// <summary>
        /// Carga las escenas correspondientes al juego principal (async)
        /// </summary>
        /// <returns>Devuelve cuando se terminan de cargar todas las escenas relacionadas</returns>
        public IEnumerator LoadMainInterface(bool loadMap)
        {
            // Inicio de fundido a negro para suavizar la carga
            yield return StartFade();

            // Carga la escena de la interfaz
            yield return WaitForLoadScene("Interface");

            // Prepara la escena del mapa, si procede
            if (loadMap)
            {
                yield return WaitForUnloadScene("StartMenu");
                yield return WaitForLoadScene("Map");
            }

            // Fin de fundido a negro para suavizar la carga
            yield return EndFade();
        }

        /// <summary>
        /// Carga las escenas correspondientes al mapa de juego (async)
        /// </summary>
        /// <returns>Devuelve cuando se termina de cargar la escena</returns>
        public IEnumerator LoadMap() {
            // Inicio de fundido a negro para suavizar la carga
            yield return StartFade();

            // Descarga todas las escenas, salvo la de gestion (Main) y la de interfaz (Interface)
            yield return WaitForUnloadScene("StartMenu");
            yield return WaitForUnloadScene("ShelterStay");
            yield return WaitForUnloadScene("AnimalShop");
            yield return WaitForUnloadScene("DecoShop");
            yield return WaitForUnloadScene("Minigames");
            // Carga las escenas del mapa (Map) y de la interfaz (Interface)
            yield return WaitForLoadScene("Map");

            // Fin de fundido a negro para suavizar la carga
            yield return EndFade();
        }

        /// <summary>
        /// Carga la escena correspondiente al selector de minijuegos (async)
        /// </summary>
        /// <returns>Devuelve cuando se termina de cargar la escena</returns>
        public IEnumerator LoadMinigames()
        {
            // Inicio de fundido a negro para suavizar la carga
            yield return StartFade();

            // Descarga todas las escenas, salvo la de gestion (Main) y la de interfaz (Interface)
            yield return WaitForUnloadScene("StartMenu");
            yield return WaitForUnloadScene("ShelterStay");
            yield return WaitForUnloadScene("AnimalShop");
            yield return WaitForUnloadScene("DecoShop");
            yield return WaitForUnloadScene("Map");
            // Carga la escena de los minijuegos
            yield return WaitForLoadScene("Minigames");

            // Fin de fundido a negro para suavizar la carga
            yield return EndFade();
        }

        /// <summary>
        /// Carga la escena correspondiente a la tienda de recursos (async)
        /// </summary>
        /// <returns>Devuelve cuando se termina de cargar la escena</returns>
        public IEnumerator LoadResourceShop() {
            yield return WaitForLoadScene("ResourcesShop");
        }

        /// <summary>
        /// Carga la escena correspondiente a la estancia del refugio (async)
        /// </summary>
        /// <param name="ID">Identificador de la estancia del refugio</param>
        /// <returns>Devuelve cuando se termina de cargar la escena</returns>
        public IEnumerator LoadShelterStay(string ID) {
            // Inicio de fundido a negro para suavizar la carga
            yield return StartFade();

            // Descarga todas las escenas, salvo la de gestion (Main) y la de interfaz (Interface)
            yield return WaitForUnloadScene("ResourcesShop");
            yield return WaitForUnloadScene("Map");
            // Carga la escena de la estancia
            yield return WaitForLoadScene("ShelterStay");

            // Fin de fundido a negro para suavizar la carga
            yield return EndFade();
        }

        /// <summary>
        /// Descarga la escena correspondiente a la tienda de recursos (async)
        /// </summary>
        /// <returns>Devuelve cuando se termina de descargar la escena</returns>
        public IEnumerator UnloadResourceShop() {
            yield return WaitForUnloadScene("ResourcesShop");
        }

        private IEnumerator StartFade() {
            blackFadeImage.gameObject.SetActive(true);
            yield return blackFadeImage.DOFade(1, fadeDuration).WaitForCompletion();
        }

        private IEnumerator EndFade() {
            yield return blackFadeImage.DOFade(0, fadeDuration).WaitForCompletion();
            blackFadeImage.gameObject.SetActive(false);
        }

        private IEnumerator WaitForLoadScene(string sceneName) {
            // Comprueba que la escena no esté ya cargada
            if (!SceneManager.GetSceneByName(sceneName).IsValid()) {
                // Lleva a cabo la carga de la escena
                AsyncOperation ao = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
                do {
                    yield return null;
                } while (!ao.isDone);

                // Notificación del evento
                if (SceneLoaded != null) {
                    SceneLoaded(sceneName);
                }
            }
        }

        private IEnumerator WaitForUnloadScene(string sceneName) {
            // Comprueba que la escena esté realmente cargada
            if (SceneManager.GetSceneByName(sceneName).IsValid()) {
                // Lleva a cabo la descarga de la escena
                AsyncOperation ao = SceneManager.UnloadSceneAsync(sceneName);
                do {
                    yield return null;
                } while (!ao.isDone);

                // Notificación del evento
                if (SceneUnloaded != null) {
                    SceneUnloaded(sceneName);
                }
            }
        }
    }

}