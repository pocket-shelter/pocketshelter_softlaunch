using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class ResourcesManager : MonoBehaviour {
        /* Eventos de notificación */
        public delegate void ResourceNotification(int resource);
        /// <summary>
        /// Notificación cuando el número de monedas se modifique
        /// </summary>
        public ResourceNotification CoinsNumberChanged;
        /// <summary>
        /// Notificación cuando el número de monedas solidarias se modifique
        /// </summary>
        public ResourceNotification SolidaryCoinsNumberChanged;

        // Variables con los recursos almacenados
        private int coins;
        private int solidaryCoins;

        /// <summary>
        /// Número de monedas
        /// </summary>
        public int Coins {
            get {
                return coins;
            }
        }

        /// <summary>
        /// Número de monedas solidarias
        /// </summary>
        public int SolidaryCoins {
            get {
                return solidaryCoins;
            }
        }

        #region Public Functions

        /// <summary>
        /// Comunica la cantidad inicial de recursos al gestor
        /// </summary>
        /// <param name="initialCoins">Número inicial de monedas</param>
        /// <param name="initialSolidaryCoins">Número inicial de monedas solidarias</param>
        public void SetInitialCount(int initialCoins, int initialSolidaryCoins) {
            coins = initialCoins;
            solidaryCoins = initialSolidaryCoins;
        }

        /// <summary>
        /// Comunica la adquisición de monedas al gestor de recursos
        /// </summary>
        /// <param name="coinsToAdd">Número de monedas a añadir</param>
        public void AddCoins(int coinsToAdd) {
            SetCoins(coins + coinsToAdd);
        }

        /// <summary>
        /// Comunica la adquisición de monedas solidarias al gestor de recursos
        /// </summary>
        /// <param name="coinsToAdd">Número de monedas solidarias a añadir</param>
        public void AddSolidaryCoins(int coinsToAdd) {
            SetSolidaryCoins(solidaryCoins + coinsToAdd);
        }

        /// <summary>
        /// Comunica el uso de monedas al gestor de recursos
        /// </summary>
        /// <param name="coinsToUse">Número de monedas a gastar</param>
        /// <returns>¿Había suficientes monedas?</returns>
        public bool UseCoins(int coinsToUse) {
            bool res = false;
            if (coins >= coinsToUse) {
                SetCoins(coins - coinsToUse);
                res = true;
            }

            return res;
        }

        /// <summary>
        /// Comunica el uso de monedas solidarias al gestor de recursos
        /// </summary>
        /// <param name="coinsToUse">Número de monedas solidarias a gastar</param>
        /// <returns>¿Había suficientes monedas solidarias?</returns>
        public bool UseSolidaryCoins(int coinsToUse) {
            bool res = false;
            if (solidaryCoins >= coinsToUse) {
                SetSolidaryCoins(solidaryCoins - coinsToUse);
                res = true;
            }

            return res;
        }

        #endregion

        private void SetCoins(int newCoins) {
            // Asignación y notificación del número de monedas
            coins = newCoins;
            if (CoinsNumberChanged != null) {
                CoinsNumberChanged(coins);
            }
        }

        private void SetSolidaryCoins(int newCoins) {
            // Asignación y notificación del número de monedas solidarias
            solidaryCoins = newCoins;
            if (SolidaryCoinsNumberChanged != null) {
                SolidaryCoinsNumberChanged(solidaryCoins);
            }
        }

    }

}