using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class AnimalsManager : MonoBehaviour {

        private List<Animal> animalsInShelter;

        /// <summary>
        /// Animales acogidos en el refugio
        /// </summary>
        public List<Animal> Animals {
            get {
                return animalsInShelter;
            }
        }

        /// <summary>
        /// Comunica la acogida de un nuevo animal
        /// </summary>
        /// <param name="newAnimal">Información del animal recién acogido</param>
        public void AddNewAnimal(Animal newAnimal) {
            // Añade un nuevo animal al refugio
            animalsInShelter.Add(newAnimal);
        }

    }

}