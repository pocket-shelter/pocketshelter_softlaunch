using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {
    public class SessionController : MonoBehaviour {
        [Header("Configuration")]
        [SerializeField]
        private int distributionTries;

        private Dictionary<string, List<string>> animalsInShelter;

        // TODO Cambiar el acceso a la variable
        public string currentBuilding;

        private void Start() {
            // Inicializacion de variables
            animalsInShelter = new Dictionary<string, List<string>>();

            // Reparte los diferentes animales por las estancias del jugador
            string shelterStay = null;
            int tryCount;
            foreach (string animalID in GameManager.PlayerData.animals) {
                // Elige la estancia en la que estara el animal
                shelterStay = null;
                tryCount = 0;
                do {
                    shelterStay = GameManager.PlayerData.buildings[Random.Range(0, GameManager.PlayerData.buildings.Count)];
                    if (animalsInShelter.ContainsKey(shelterStay)) {
                        // Existe algun animal en la estancia, luego se comprueba si hay capacidad suficiente
                        if (animalsInShelter[shelterStay].Count + 1 > GameManager.PlayerData.GetBuildingCapacity(shelterStay)) {
                            // La estancia elegida esta completa
                            shelterStay = null;
                            ++tryCount;
                        }
                    } else {
                        // No hay ningun animal en la estancia, luego el animal puede estar ahi
                        animalsInShelter.Add(shelterStay, new List<string>());
                    }
                } while ((shelterStay == null) && (tryCount < distributionTries));

                // Anade el animal a la estancia
                if (shelterStay != null) {
                    animalsInShelter[shelterStay].Add(animalID);
                    Debug.Log("El animal " + animalID + " va a la estancia " + shelterStay);
                } else {
                    Debug.Log("No se ha podido alojar el animal " + animalID);
                }

            }
        }

        public List<string> GetAnimalsForCurrentBuilding() {
            List<string> res = new List<string>();
            if (animalsInShelter.ContainsKey(currentBuilding)) {
                res = animalsInShelter[currentBuilding];
            }

            return res;
        }
    }

}