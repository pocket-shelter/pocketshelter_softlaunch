using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class StartMenuController : MonoBehaviour {

        public void PlayButtonPressed() {
            // Carga el juego
            GameManager.LoadInterface();
        }
    }

}