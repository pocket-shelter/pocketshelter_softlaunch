using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace PerlaOculta.PocketShelter {

    public class ShelterStayUIController : MonoBehaviour {

        [SerializeField]
        private GameObject animalSelectionPanel;

        [SerializeField]
        private TextMeshProUGUI animalNameText;

        public void CloseAnimalSelectionPanel() {
            // Oculta la interfaz con el nombre del animal
            animalSelectionPanel.SetActive(false);
        }

        public void OpenAnimalSelectionPanel(string animalName) {
            // Muestra la interfaz con el nombre del animal
            animalSelectionPanel.SetActive(true);
            animalNameText.text = "¡Soy " + animalName + "!";
        }
    }

}