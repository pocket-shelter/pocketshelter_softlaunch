using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace PerlaOculta.PocketShelter {

    public class InterfaceManager : MonoBehaviour {
        [SerializeField]
        private TextMeshProUGUI coinsText;

        [SerializeField]
        private TextMeshProUGUI solidaryCoinsText;

        [Space]

        [SerializeField]
        private GameObject mapButton;

        [SerializeField]
        private GameObject minigamesButton;

        #region Unity Messages

        private void Start() {
            UpdateResourceCoins(GameManager.ResourcesManager.Coins);
            UpdateResourceSolidaryCoins(GameManager.ResourcesManager.SolidaryCoins);
        }

        private void OnEnable() {
            GameManager.ResourcesManager.CoinsNumberChanged += UpdateResourceCoins;
            GameManager.ResourcesManager.SolidaryCoinsNumberChanged += UpdateResourceSolidaryCoins;
            GameManager.SceneController.SceneLoaded += UpdateUILoadedScene;
            GameManager.SceneController.SceneUnloaded += UpdateUIUnloadedScene;
        }

        private void OnDisable() {
            GameManager.ResourcesManager.CoinsNumberChanged -= UpdateResourceCoins;
            GameManager.ResourcesManager.SolidaryCoinsNumberChanged -= UpdateResourceSolidaryCoins;
            GameManager.SceneController.SceneLoaded -= UpdateUILoadedScene;
            GameManager.SceneController.SceneUnloaded -= UpdateUIUnloadedScene;
        }

        #endregion

        #region Public Messages

        public void GoToMap() {
            // Carga el mapa del juego
            GameManager.LoadMap();
        }

        public void GoToMinigames()
        {
            // Carga los minijuegos del juego
            GameManager.LoadMinigames();
        }

        public void ShowCoinsPanel() {
            GameManager.LoadResourcesShop();
        }

        #endregion

        #region Events Notifications

        private void UpdateResourceCoins(int count) {
            if (count < 1000000) {
                coinsText.text = count.ToString();
            } else {
                coinsText.text = (count/1000).ToString() + "K";
            }
        }

        private void UpdateResourceSolidaryCoins(int solidaryCoins) {
            if (solidaryCoins < 1000000) {
                solidaryCoinsText.text = solidaryCoins.ToString();
            } else {
                solidaryCoinsText.text = (solidaryCoins / 1000).ToString() + "K";
            }
        }

        private void UpdateUILoadedScene(string scene) {
            // Comprueba la escena que se acaba de cargar
            switch (scene)
            {
                case "Map":
                    mapButton.SetActive(false);
                    break;
                case "Minigames":
                    minigamesButton.SetActive(false);
                    break;
            }
        }

        private void UpdateUIUnloadedScene(string scene)
        {
            // Comprueba la escena que se acaba de descargar
            switch (scene)
            {
                case "Map":
                    mapButton.SetActive(true);
                    break;
                case "Minigames":
                    minigamesButton.SetActive(true);
                    break;
            }
        }

        #endregion
    }

}