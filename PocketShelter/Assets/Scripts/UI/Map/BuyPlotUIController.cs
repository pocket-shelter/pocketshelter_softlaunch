using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace PerlaOculta.PocketShelter {

    public class BuyPlotUIController : MonoBehaviour {
        [Header("References")]
        [Tooltip("Referencia al componente Text con el nombre de la parcela")]
        [SerializeField]
        private TextMeshProUGUI nameText;
        [Tooltip("Referencia al componente Text con el precio de la parcela")]
        [SerializeField]
        private TextMeshProUGUI priceText;

        // Callbacks de ejecución
        private System.Action positiveCallback;
        private System.Action negativeCallback;

        #region Unity Messages

        private void Update() {
            // TODO Implementar correctamente con eventos de input, comprobando que el click se hace fuera del panel
            if (Input.GetMouseButtonDown(1)) {
                ButtonNoPressed();
            }
        }

        #endregion

        #region UI Events

        /// <summary>
        /// Evento de interfaz al confirmar la compra
        /// </summary>
        public void ButtonYesPressed() {
            // Notificación del callback y desactivación de la interfaz
            if (positiveCallback != null) {
                positiveCallback();
            }
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Evento de interfaz al cancelar la compra
        /// </summary>
        public void ButtonNoPressed() {
            // Notificación del callback y desactivación de la interfaz
            if (negativeCallback != null) {
                negativeCallback();
            }
            gameObject.SetActive(false);
        }

        #endregion

        #region Public Messages

        public void Show(string name, int price, System.Action positiveCB, System.Action negativeCB, string plotID) {
            nameText.text = name.ToString();
            priceText.text = price.ToString();
            positiveCallback = positiveCB;
            negativeCallback = negativeCB;
            gameObject.SetActive(true);
            Debug.Log("showing " + plotID + " info");
        }

        #endregion
    }

}