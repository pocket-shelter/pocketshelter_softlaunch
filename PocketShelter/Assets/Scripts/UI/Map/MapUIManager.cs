using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class MapUIManager : MonoBehaviour {
        [Header("References")]

        [SerializeField]
        [Tooltip("Referencia al controlador de la interfaz de compra de las estancias")]
        private BuyPlotUIController buyPlotUIController;

        [SerializeField]
        [Tooltip("Referencia al controlador de la interfaz de visita/mejora de las estancias")]
        private VisitBuildingUIController visitPlotUIController;

        [SerializeField]
        [Tooltip("Referencia al controlador de la interfaz de visita de los edificios")]
        private VisitBuildingUIController visitBuildingUIController;

        [SerializeField]
        [Tooltip("Referencia al Transform de la camara para la miniatura de los edificios en la interfaz")]
        private Transform buildingCamera;

        public void ShowBuyPlotUI(MapPlot plotInfo, System.Action buyCallback) {
            // Coloca la camara para la miniatura
            Vector3 pos = buildingCamera.position;
            pos.x = plotInfo.X * 10;
            pos.y = plotInfo.Y * 10;
            buildingCamera.position = pos;

            // Muestra la interfaz de compra
            buyPlotUIController.Show(plotInfo.name, plotInfo.price1, buyCallback, null, plotInfo.ID);
        }

        public void ShowVisitPlotUI(MapPlot plotInfo, System.Action visitCallback, System.Action upgradeCallback) {
            // Coloca la camara para la miniatura
            Vector3 pos = buildingCamera.position;
            pos.x = plotInfo.X * 10;
            pos.y = plotInfo.Y * 10;
            buildingCamera.position = pos;

            // Muestra la interfaz de visita/mejora segun el nivel actual del edificio
            switch (GameManager.PlayerData.GetConstructionLevel(plotInfo.ID)) {
                case 1:
                    visitBuildingUIController.Show(plotInfo.name, plotInfo.price2, 1, plotInfo.capacity1, visitCallback, upgradeCallback, plotInfo.ID);
                    break;
                case 2:
                    visitBuildingUIController.Show(plotInfo.name, plotInfo.price3, 2, plotInfo.capacity2, visitCallback, upgradeCallback, plotInfo.ID);
                    break;
                case 3:
                    visitBuildingUIController.Show(plotInfo.name, 0, 3, plotInfo.capacity3, visitCallback, upgradeCallback, plotInfo.ID);
                    break;
            }
            
        }

        public void ShowVisitBuildingUI(MapBuilding buildingInfo, System.Action visitCallback) {
            //visitBuildingUIController.Show(buildingInfo.name, visitCallback);
        }

    }

}