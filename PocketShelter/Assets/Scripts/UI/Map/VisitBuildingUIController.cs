using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace PerlaOculta.PocketShelter {

    public class VisitBuildingUIController : MonoBehaviour {
        [Header("References")]
        [Tooltip("Referencia al componente Text con el nombre del edificio")]
        [SerializeField]
        private TextMeshProUGUI nameText;
        [Tooltip("Referencia al componente Text con el precio de mejora del edificio")]
        [SerializeField]
        private TextMeshProUGUI priceText;
        [Tooltip("Referencia al componente Text con el nivel de construcción del edificio")]
        [SerializeField]
        private TextMeshProUGUI levelText;
        [Tooltip("Referencia al componente Text con la capacidad del edificio")]
        [SerializeField]
        private TextMeshProUGUI capacityText;

        [Space]

        [Tooltip("Referencia al botón de mejora del edificio")]
        [SerializeField]
        private GameObject upgradeButton;

        // Callbacks de ejecución
        private System.Action positiveCallback;
        private System.Action upgradeCallback;

        #region Unity Messages

        private void Update() {
            // TODO Implementar correctamente con eventos de input, comprobando que el click se hace fuera del panel
            if (Input.GetMouseButtonDown(1)) {
                gameObject.SetActive(false);
            }
        }

        #endregion

        #region UI Events

        /// <summary>
        /// Evento de interfaz al confirmar la visita
        /// </summary>
        public void ButtonEnterPressed() {
            // Notificación del callback y desactivación de la interfaz
            if (positiveCallback != null) {
                positiveCallback();
            }
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Evento de interfaz al solicitar la mejora
        /// </summary>
        public void ButtonUpgradePressed() {
            // Notificación del callback y desactivación de la interfaz
            if (upgradeCallback != null) {
                upgradeCallback();
            }
            gameObject.SetActive(false);
        }

        #endregion

        #region Public Messages

        /// <summary>
        /// Muestra la interfaz de visita del edificio determinado
        /// </summary>
        /// <param name="name">Nombre del edificio</param>
        /// <param name="price">Precio de mejora del edificio</param>
        /// <param name="level">Nivel de construcción actual</param>
        /// <param name="capacity">Capacidad actual</param>
        /// <param name="visitCB">Callback, si procede, para la visita al edificio</param>
        /// <param name="upgradeCB">Callback, si procede, para la mejora del edificio</param>
        /// <param name="buildingID"></param>
        public void Show(string name, int price, int level, int capacity, System.Action visitCB, System.Action upgradeCB, string buildingID) {
            gameObject.SetActive(true);
            nameText.text = name;
            if (price == 0) {
                // Edificio sin mejora disponible
                priceText.gameObject.SetActive(false);
                upgradeButton.SetActive(false);
            } else {
                // Edificio con mejora disponible
                priceText.gameObject.SetActive(true);
                upgradeButton.SetActive(true);
                priceText.text = price.ToString();
            }
            // TODO Extraer cadena de texto del código
            levelText.text = "NIVEL " + level.ToString();
            capacityText.text = "/ " + capacity.ToString();
            positiveCallback = visitCB;
            upgradeCallback = upgradeCB;
            Debug.Log("checking " + buildingID + " info");
        }

        #endregion
    }

}