﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class InputManager : MonoBehaviour {
        /* Diferentes inputs que pueden ser utilizados por el jugador */
        public enum InputType {
            Mouse, Touch_Mobile
        }

        /* Diferentes esquemas de input que existen en el juego */
        public enum InputScheme {
            ShelterStay, ShelterStay_Deco
        }

        /* Singleton */
        private static InputManager instance;

        #region Input Delegates

        // Delegados del InputManager
        public delegate void InputOrder();
        public delegate void InputOrderFloat(float value);
        public delegate void InputOrderBool(bool value);
        public delegate void InputOrderInt(int value);
        public delegate void InputOrderV2(Vector2 vector2);
        public delegate void InputOrderFloatV2(float value, Vector2 vector2);
        public delegate void InputTypeChange(InputType oldType, InputType newType);
        public delegate void InputSchemeChange(InputScheme oldScheme, InputScheme newScheme);

        #endregion

        #region Basic Input Events

        /// <summary>
        /// Evento para notificar que se ha pulsado algún botón/tecla o en la pantalla táctil
        /// </summary>
        public static InputOrder AnyButtonPressed;

        /// <summary>
        /// Evento para notificar el movimiento de la cámara
        /// </summary>
        public static InputOrderV2 CameraMovement;

        /// <summary>
        /// Evento para notificar que el jugador ha seleccionado un punto de la pantalla
        /// </summary>
        public static InputOrderV2 ScreenClick;

        /// <summary>
        /// Evento para notificar que se ha modificado el esquema de entrada
        /// </summary>
        public static InputSchemeChange InputSchemeChanged;

        /// <summary>
        /// Evento para notificar que se ha modificado el tipo de input del jugador
        /// </summary>
        public static InputTypeChange InputTypeChanged;

        /// <summary>
        /// Evento para hacer zoom en el tablero
        /// </summary>
        public static InputOrderFloatV2 Zoom;

        #endregion

        [Header("References")]
        [Tooltip("Referencias a los diferentes controladores del input")]
        [SerializeField]
        private List<MonoBehaviour> inputControllers;

        [Space]

        /// <summary>
        /// ¿Está habilitado el input del jugador?
        /// </summary>
        private bool inputEnabled;

        /// <summary>
        /// Tipo de input utilizado por el jugador
        /// </summary>
        private InputType inputType;

        /// <summary>
        /// Esquema de input actual
        /// </summary>
        private InputScheme inputScheme;

        #region Unity Messages

        private void Awake() {
            /* Singleton */
            if (instance == null) {
                instance = this;
            } else if (!instance.Equals(this)) {
                Destroy(gameObject);
            }
        }

        private void Start() {
            // Input desactivado
            SetEnabled(true);
            // Tipo de input a utilizar (por defecto, ratón)
            Type = InputType.Mouse;
            // Tipo de esquema (por defecto, menú)
            Scheme = InputScheme.ShelterStay;
        }

        #endregion

        #region Event Notifications

        /// <summary>
        /// Notificación de evento sin parámetro
        /// </summary>
        /// <param name="inputOrder">Evento a notificar</param>
        public static void NotifyEvent(InputOrder inputOrder) {
            if (instance.inputEnabled && (inputOrder != null)) {
                inputOrder();
            }
        }

        /// <summary>
        /// Notificación de evento con parámetro 'float'
        /// </summary>
        /// <param name="inputOrder">Evento a notificar</param>
        /// <param name="value">Parámetro del evento</param>
        public static void NotifyEvent(InputOrderFloat inputOrder, float value) {
            if (instance.inputEnabled && (inputOrder != null)) {
                inputOrder(value);
            }
        }

        /// <summary>
        /// Notificación de evento con parámetro 'Vector2'
        /// </summary>
        /// <param name="inputOrder">Evento a notificar</param>
        /// <param name="value">Parámetro del evento</param>
        public static void NotifyEvent(InputOrderV2 inputOrder, Vector2 value) {
            if (instance.inputEnabled && (inputOrder != null)) {
                inputOrder(value);
            }
        }

        /// <summary>
        /// Notificación de evento con parámetro 'bool'
        /// </summary>
        /// <param name="inputOrder">Evento a notificar</param>
        /// <param name="value">Parámetro del evento</param>
        public static void NotifyEvent(InputOrderBool inputOrder, bool value) {
            if (instance.inputEnabled && (inputOrder != null)) {
                inputOrder(value);
            }
        }

        /// <summary>
        /// Notificación de evento con parámetro 'int'
        /// </summary>
        /// <param name="inputOrder">Evento a notificar</param>
        /// <param name="value">Parámetro del evento</param>
        public static void NotifyEvent(InputOrderInt inputOrder, int value) {
            if (instance.inputEnabled && (inputOrder != null)) {
                inputOrder(value);
            }
        }

        /// <summary>
        /// Notificación de evento con parámetros 'float' y 'Vector2'
        /// </summary>
        /// <param name="inputOrder">Evento a notificar</param>
        /// <param name="value">Parámetro 'float' del evento</param>
        /// <param name="vector2">Parámetro 'Vector2' del evento</param>
        public static void NotifyEvent(InputOrderFloatV2 inputOrder, float value, Vector2 vector2) {
            if (instance.inputEnabled && (inputOrder != null)) {
                inputOrder(value, vector2);
            }
        }

        #endregion

        #region Extra Input Information

        public static void SetEnabled(bool enabled) {
            // Habilita/Deshabilita el input del jugador, haciendo lo propio con los controladores particulares
            instance.inputEnabled = enabled;
            foreach (MonoBehaviour ct in instance.inputControllers) {
                ct.enabled = enabled;
            }
        }

        /// <summary>
        /// Tipo de esquema de input actual
        /// </summary>
        public static InputScheme Scheme {
            get {
                return instance.inputScheme;
            }
            set {
                // Notificación de cambio antes de la asignación, si procede
                if (!instance.inputScheme.Equals(value)) {
                    if (InputSchemeChanged != null) {
                        InputSchemeChanged(instance.inputScheme, value);
                    }
                }
                instance.inputScheme = value;
            }
        }

        /// <summary>
        /// Tipo de input utilizado por el jugador
        /// </summary>
        public static InputType Type {
            get {
                return instance.inputType;
            }
            set {
                // Notificación de cambio antes de la asignación, si procede
                if (!instance.inputType.Equals(value)) {
                    if (InputTypeChanged != null) {
                        InputTypeChanged(instance.inputType, value);
                    }
                }
                instance.inputType = value;
            }
        }

        #endregion
        
    }

}