﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class MouseInput : MonoBehaviour {
        [Tooltip("Sensibilidad a la hora de considerar si el jugador está desplazando el click")]
        [Range(0.0f, 10.0f)]
        public float dragSensibility;

        /// <summary>
        /// Última posición del ratón registrada
        /// </summary>
        private Vector3 lastMousePosition;

        /// <summary>
        /// ¿Se está moviendo la cámara?
        /// </summary>
        private bool movingCamera;



        // Variables auxiliares
        private float auxFloat;
        private Vector2 auxVector2;

        public bool game;

        #region Unity Messages

        private void Awake() {
            // Establece el modo de input
            // TODO Gestionar que solo se haga cuando realmente se esté utilizando el ratón
            InputManager.Type = InputManager.InputType.Mouse;
        }

        private void Start() {

        }

        private void Update() {
            /* Input general del juego */

            // Comprueba si el jugador ha hecho algún clic con el ratón
            if ((Input.GetMouseButtonUp(0)) || (Input.GetMouseButtonUp(1)) || (Input.GetMouseButtonUp(2))) {
                InputManager.NotifyEvent(InputManager.AnyButtonPressed);
            }

            /* Input específico de pantalla */
            switch (InputManager.Scheme) {
                case InputManager.InputScheme.ShelterStay:
                    ShelterStayUpdate();
                    break;
                case InputManager.InputScheme.ShelterStay_Deco:
                    ShelterStayDecoUpdate();
                    break;
            }
        }

        #endregion

        private void ShelterStayUpdate() {
            // Comprueba si el jugador ha iniciado un click sobre la pantalla para registrar la posición
            if (Input.GetMouseButtonDown(0)) {
                lastMousePosition = Input.mousePosition;
            }

            // Comprueba si el jugador está haciendo click sobre la pantalla
            if (Input.GetMouseButton(0)) {
                // Comprueba si el click está en movimiento, para notificar así el movimiento de cámara
                auxVector2 = Input.mousePosition - lastMousePosition;
                if (auxVector2.magnitude > dragSensibility) {
                    movingCamera = true;
                    InputManager.NotifyEvent(InputManager.CameraMovement, -auxVector2);
                }

                // Actualiza la última posición del cursor
                lastMousePosition = Input.mousePosition;
            }

            // Comprueba si el jugador ha terminado un click sobre la pantalla
            if (Input.GetMouseButtonUp(0)) {
                // Se lleva a cabo la notificación solo si no se ha movido la cámara
                if (!movingCamera) {
                    InputManager.NotifyEvent(InputManager.ScreenClick, Input.mousePosition);
                }
                // Reinicia las variables de control
                movingCamera = false;
            }
        }

        private void ShelterStayDecoUpdate() {
            // TODO Controlar el movimiento de los objetos
        }

    }

}