using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace PerlaOculta.PocketShelter {

    public class PatrolController : MonoBehaviour {
        [SerializeField]
        private GameObject patrolButton;

        [SerializeField]
        private GameObject animalPanel;

        [SerializeField]
        private TextMeshProUGUI animalText;

        void Start() {
            // TODO Comprobar si la sala de admision ya esta completa

            // Comprueba si el jugador no tiene suficiente espacio para acoger un nuevo animal para deshabilitar la opcion al jugador
            if (GameManager.PlayerData.GetCurrentCapacity() <= 0) {
                patrolButton.SetActive(false);
            }
        }

        public void Patrol() {
            // Elige un animal para el jugador
            Animal animal = FosterAnimal();

            // Anade el animal a la info del jugador y lo muestra en la interfaz
            GameManager.PlayerData.AddAnimal(animal.ID);
            animalText.text = "¡Has encontrado a " + animal.name + "!";
            animalPanel.SetActive(true);

            // TODO Anadir el animal a la sala de admision

            // Comprueba si el jugador no tiene suficiente espacio para acoger un nuevo animal para deshabilitar la opcion al jugador
            if (GameManager.PlayerData.GetCurrentCapacity() <= 0) {
                patrolButton.SetActive(false);
            }
        }

        public void CloseAnimalUI() {
            // Desactiva interfaz
            animalPanel.SetActive(false);
        }

        private Animal FosterAnimal() {
            // TODO Comprobar que el jugador no tenga ya dicho animal
            return GameManager.GameData.animals[Random.Range(0, GameManager.GameData.animals.Count)];
        }
    }

}
