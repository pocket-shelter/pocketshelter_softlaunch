using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    [System.Serializable]
    public class Animal {
        public enum Type { Cat, Dog }

        public string ID;

        public string name;
        public Type animal;
        public string description;
    }

    [System.Serializable]
    public class MapBuilding {

        public string ID;

        public string name;

        public int X;
        public int Y;

        public string description;

    }

    [System.Serializable]
    public class MapPlot {

        public string ID;

        public string name;

        public int X;
        public int Y;

        public int price1;
        public int capacity1;

        public int price2;
        public int capacity2;

        public int price3;
        public int capacity3;

    }

    [CreateAssetMenu(menuName = "PocketShelter/GameConfiguration/Once/Data")]
    public class PocketShelterData : ScriptableObject {

        public List<Animal> animals;

        public List<MapBuilding> buildings;

        public List<MapPlot> plots;

        public Animal GetAnimal(string ID)
        {
            Animal res = null;
            int a = 0;
            while ((res == null) && (a < animals.Count))
            {
                if (animals[a].ID.Equals(ID))
                {
                    res = animals[a];
                }
                a++;
            }

            return res;
        }

        public MapBuilding GetMapBuilding(string ID)
        {
            MapBuilding res = null;
            int mb = 0;
            while ((res == null) && (mb < buildings.Count))
            {
                if (buildings[mb].ID.Equals(ID))
                {
                    res = buildings[mb];
                }
                mb++;
            }

            return res;
        }

        public MapPlot GetMapPlot(string ID)
        {
            MapPlot res = null;
            int mp = 0;
            while ((res == null) && (mp < plots.Count))
            {
                if (plots[mp].ID.Equals(ID))
                {
                    res = plots[mp];
                }
                mp++;
            }

            return res;
        }

    }

}