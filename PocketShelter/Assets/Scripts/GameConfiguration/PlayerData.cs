using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    [CreateAssetMenu(menuName = "PocketShelter/GameConfiguration/Once/PlayerData")]
    public class PlayerData : ScriptableObject {
        [Tooltip("Monedas del jugador")]
        public int coins;

        [Tooltip("Monedas solidarias del jugador")]
        public int solidaryCoins;

        [Tooltip("Edificios (IDs) construidos por el jugador")]
        public List<string> buildings;

        [Tooltip("Nivel de construcción de los edificios")]
        public List<int> constructionLevels;

        [Tooltip("Animales (IDs) acogidos por el jugador")]
        public List<string> animals;

        public void AddAnimal(string animalID)
        {
            // Comprueba si existe información previa sobre los animales
            if (animals == null)
            {
                animals = new List<string>();
            }

            // Añade el animal al listado
            animals.Add(animalID);
        }

        public void ConstructBuilding(string ID) {
            // Comprueba si existe información previa sobre los edificios
            if (buildings == null) {
                buildings = new List<string>();
                constructionLevels = new List<int>();
            }

            // Comprueba si ya se ha construido el edificio
            if (buildings.Contains(ID)) {
                // Incrementa el nivel del edificio
                ++constructionLevels[buildings.FindIndex((x) => (x == ID))];
            } else {
                // Construye por primera vez el edificio
                buildings.Add(ID);
                constructionLevels.Add(1);
            }
        }

        public int GetBuildingCapacity(string buildingID)
        {
            // Devuelve la capacidad del edificio, identificando previamente su índice
            return GetBuildingCapacityByIndex(buildings.FindIndex((x) => (x == buildingID)));
        }

        public int GetConstructionLevel(string ID) {
            int res = 0;
            // Comprueba si el edificio está construido
            if (buildings.Contains(ID)) {
                // Obtiene el nivel de construcción
                res = constructionLevels[buildings.FindIndex((x) => (x == ID))];
            }

            return res;
        }

        public int GetCurrentCapacity()
        {
            // Recorre todo el listado de los edificios construidos por el jugador
            int totalCapacity = 0;
            for (int b=0; b<buildings.Count;b++ )
            {
                totalCapacity += GetBuildingCapacityByIndex(b);
            }

            return (totalCapacity - animals.Count);
        }

        private int GetBuildingCapacityByIndex(int index)
        {
            int res = 0;
            // Obtiene la información del edificio construido
            MapPlot mapPlot = GameManager.GameData.GetMapPlot(buildings[index]);

            // Comprueba la capacidad actual del edificio según su nivel de construcción
            switch (constructionLevels[index])
            {
                case 1:
                    res = mapPlot.capacity1;
                    break;
                case 2:
                    res = mapPlot.capacity2;
                    break;
                case 3:
                    res = mapPlot.capacity3;
                    break;
            }

            return res;
        }
    }

}