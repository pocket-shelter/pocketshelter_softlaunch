using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PerlaOculta.PocketShelter {

    public class GameConfigurationLoader : MonoBehaviour {
        public enum DataToLoad { Animals, Buildings, Plots };

        [SerializeField]
        private bool syncData;

        [Header("URLs")]
        [SerializeField]
        private string animalsURL;
        [SerializeField]
        private string buildingsURL;
        [SerializeField]
        private string plotsURL;

        [Header("References")]
        [Tooltip("Referencia al controlador de escenas")]
        [SerializeField]
        private SceneController sceneController;

        [Space]

        [Tooltip("Referencia a la configuración del juego")]
        [SerializeField]
        private PocketShelterData data;

        private int dataLoaded;

        void Start() {
            if (syncData)
            {
                dataLoaded = 0;
                StartCoroutine(DownloadAndImport(animalsURL, DataToLoad.Animals));
                StartCoroutine(DownloadAndImport(buildingsURL, DataToLoad.Buildings));
                StartCoroutine(DownloadAndImport(plotsURL, DataToLoad.Plots));
            } else
            {
                dataLoaded = 3;
            }
        }

        private void Update() {
            if (dataLoaded == 3) {
                StartCoroutine(sceneController.LoadGame());
                dataLoaded = 0;
            } else if (Time.time > 5) {
                Debug.LogError("Timeout al sincronizar información online");
            }
        }

        private IEnumerator DownloadAndImport(string url, DataToLoad option) {
            UnityWebRequestAsyncOperation wwwOp = UnityWebRequest.Get(url).SendWebRequest();
            do {
                yield return null;
            } while (!wwwOp.isDone);

            if (wwwOp.webRequest.error != null) {
                Debug.Log("UnityWebRequest.error:" + wwwOp.webRequest.error);
            } else if (wwwOp.webRequest.downloadHandler.text == "" || wwwOp.webRequest.downloadHandler.text.IndexOf("<!DOCTYPE") != -1) {
                Debug.Log("Uknown Format:" + wwwOp.webRequest.downloadHandler.text);
            } else {
                switch (option) {
                    case DataToLoad.Animals:
                        // Animales
                        ImportAnimalsData(wwwOp.webRequest.downloadHandler.text);
                        break;
                    case DataToLoad.Buildings:
                        // Edificios
                        ImportMapBuildingsData(wwwOp.webRequest.downloadHandler.text);
                        break;
                    case DataToLoad.Plots:
                        // Parcelas
                        ImportMapPlotsData(wwwOp.webRequest.downloadHandler.text);
                        break;
                }
            }
        }

        private void ImportAnimalsData(string text) {
            List<string[]> rows = CSVSerializer.ParseCSV(text);
            if (rows != null) {
                data.animals = new List<Animal>(CSVSerializer.Deserialize<Animal>(rows));
                ++dataLoaded;
            }
        }

        private void ImportMapBuildingsData(string text) {
            List<string[]> rows = CSVSerializer.ParseCSV(text);
            if (rows != null) {
                data.buildings = new List<MapBuilding>(CSVSerializer.Deserialize<MapBuilding>(rows));
                ++dataLoaded;
            }
        }

        private void ImportMapPlotsData(string text) {
            List<string[]> rows = CSVSerializer.ParseCSV(text);
            if (rows != null) {
                data.plots = new List<MapPlot>(CSVSerializer.Deserialize<MapPlot>(rows));
                ++dataLoaded;
            }
        }
    }

}