using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class MapBuildingController : MonoBehaviour {
        public MapBuilding info;

        private MapUIManager mapUIManager;

        private void OnMouseDown() {
            // Lanza el evento de interfaz
            mapUIManager.ShowVisitBuildingUI(info, BuildingVisited);
        }

        public void SetInfo(MapBuilding mb, MapUIManager managerRef) {
            info = mb;
            mapUIManager = managerRef;
        }

        private void BuildingVisited() {
            GameManager.LoadBuilding(info.ID);
        }

    }

}