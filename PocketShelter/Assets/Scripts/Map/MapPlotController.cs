using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class MapPlotController : MonoBehaviour {

        private int buildingLevel;

        private int nextPrice;

        private MapUIManager mapUIManager;

        private MapPlot plotInfo;

        private void OnMouseDown() {
            // Lanza el evento de interfaz según el estado de la parcela
            if (buildingLevel > 0) {
                mapUIManager.ShowVisitPlotUI(plotInfo, PlotVisited, PlotUpgraded);
            } else {
                mapUIManager.ShowBuyPlotUI(plotInfo, PlotBought);
            }
        }

        public void SetInfo(MapPlot info, MapUIManager managerRef) {
            name = info.ID;
            plotInfo = info;
            nextPrice = plotInfo.price1;
            mapUIManager = managerRef;
            buildingLevel = GameManager.PlayerData.GetConstructionLevel(info.ID);
            if (buildingLevel > 0) {
                GetComponentInChildren<SpriteRenderer>().color = Color.green;
            }
        }

        private void CalculateNewPrice() {
            // Comprueba que no se ha llegado al límite de construcción
            if (buildingLevel < 3) {
                switch (buildingLevel) {
                    case 0:
                        nextPrice = plotInfo.price1;
                        break;
                    case 1:
                        nextPrice = plotInfo.price2;
                        break;
                    case 2:
                        nextPrice = plotInfo.price3;
                        break;
                }
            }
        }

        private void PlotBought() {
            if (GameManager.ResourcesManager.Coins >= nextPrice) {
                GameManager.ResourcesManager.UseCoins(nextPrice);
                GameManager.PlayerData.ConstructBuilding(plotInfo.ID);
                ++buildingLevel;
                GetComponentInChildren<SpriteRenderer>().color = Color.green;

                // Cálculo del nuevo precio
                CalculateNewPrice();
            } else {
                Debug.LogWarning("no hay monedas suficientes para comprar");
            }
        }

        private void PlotUpgraded() {
            if (GameManager.ResourcesManager.Coins >= nextPrice) {
                GameManager.ResourcesManager.UseCoins(nextPrice);
                GameManager.PlayerData.ConstructBuilding(plotInfo.ID);
                ++buildingLevel;

                // Cálculo del nuevo precio
                CalculateNewPrice();
            } else {
                Debug.LogWarning("no hay monedas suficientes para mejorar");
            }
        }

        private void PlotVisited() {
            GameManager.LoadShelterStay(name);
        }

    }

}