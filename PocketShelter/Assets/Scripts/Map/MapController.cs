using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerlaOculta.PocketShelter {

    public class MapController : MonoBehaviour {
        [Header("References")]
        [Tooltip("Referencia a la configuración del mapa, con las diferentes parcelas y edificaciones")]
        [SerializeField]
        private PocketShelterData map;
        [Space]
        [Tooltip("Referencia al prefab de las parcelas")]
        [SerializeField]
        private GameObject plotPrefab;
        [Tooltip("Referencia al prefab de los edificios")]
        [SerializeField]
        private GameObject buildingPrefab;

        #region Unity Messages

        private void Start() {
            // Referencia al controlador de la interfaz en el mapa
            MapUIManager mapUI = FindObjectOfType<MapUIManager>();

            // Genera las parcelas y edificaciones correspondientes
            MapPlotController mpc;
            foreach (MapPlot mp in map.plots) {
                mpc = Instantiate(plotPrefab, transform).GetComponent<MapPlotController>();
                mpc.transform.localPosition = new Vector3(10 * mp.X, 10 * mp.Y, 0);
                mpc.SetInfo(mp, mapUI);
            }
            MapBuildingController mbc;
            foreach (MapBuilding mb in map.buildings) {
                mbc = Instantiate(buildingPrefab, transform).GetComponent<MapBuildingController>();
                mbc.transform.localPosition = new Vector3(10 * mb.X, 10 * mb.Y, 0);
                mbc.SetInfo(mb, mapUI);
            }
        }

        #endregion
    }

}